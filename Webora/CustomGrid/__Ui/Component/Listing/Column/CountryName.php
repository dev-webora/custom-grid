<?php
namespace Webora\CustomGrid\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Directory\Api\CountryInformationAcquirerInterface;
#use Magento\Catalog\Api\ProductRepositoryInterface;

class CountryName extends Column {
    
    private $countryInformationAcquirerInterface;
    
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = [],
        CountryInformationAcquirerInterface $countryInformationAcquirerInterface
        ){
            $this->countryInformationAcquirerInterface = $countryInformationAcquirerInterface;
            parent::__construct($context, $uiComponentFactory, $components, $data);
    }
    
    
    public function prepareDataSource(array $dataSource)
    {return $dataSource;
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                #$order = $this->_orderModel->load($item['order_id']);
                $item['shipping_country'] = $this->getCountryName($item['shipping_country']);
            }
        }
        return $dataSource;
    }
    
     private function getCountryName($countryCode, $type="local"){
        $countryName = null;
        try {
            $data = $this->countryInformationAcquirerInterface->getCountryInfo($countryCode);
            if($type == "local"){
                $countryName = $data->getFullNameLocale();
            }else {
                $countryName = $data->getFullNameLocale();
            }
        } catch (NoSuchEntityException $e) {}
        return $countryName;
    }
}