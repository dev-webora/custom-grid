<?php
namespace Webora\CustomGrid\Plugin;

use Magento\Framework\View\Element\UiComponent\DataProvider\CollectionFactory;
// use Magento\Sales\Model\ResourceModel\Order\Invoice\Collection as InvoiceGridCollection;
use Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory as InvoiceGridCollection;

class SalesInvoiceGridCollection
{
    private $collection;
    protected $logger;

    public function __construct(
        InvoiceGridCollection $collection,
        \Psr\Log\LoggerInterface $logger) {
        $this->collection = $collection;
        $this->logger = $logger;
    }

    public function aroundGetReport(CollectionFactory $subject, \Closure $proceed, $requestName) {
        $result = $proceed($requestName);

        $this->logger->info($requestName);
        
        
        // var_dump($this->collection->getData()); 
        
        // foreach($this->collection->getData() as $data){
        //     var_dump($data['base_grand_total']); die();
        // }
        
        $this->logger->info(get_class($result));
        $this->logger->info(get_class($this->collection));
        // $this->logger->info($this->collection->getSelect());
        
        $this->logger->info(intval($result instanceof $this->collection));
        
        if ($requestName == 'sales_order_invoice_grid_data_source') {
            if ($result instanceof $this->collection) {
                $select = $this->collection->getSelect();
                $select->joinLeft(["soa" => $this->collection->getTable("sales_order_address")],'main_table.entity_id = soa.parent_id AND soa.address_type="shipping"', array('country_id'))->distinct();
                $this->logger->info($this->collection->getSelect());
                return $this->collection;
            }
        }
       
        return $result;
    }
}