<?php
/**
 * @category   Webora
 * @package    Webora_CustomGrid
 * @author     Giuseppe Pisani //Webora dev
 * @copyright  Copyright (c) Webora (https://webora.it)
 * @license    https://webora.it/license.html
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Webora_CustomGrid',
    __DIR__
);
