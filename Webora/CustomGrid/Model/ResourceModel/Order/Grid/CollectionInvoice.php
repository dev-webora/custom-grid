<?php
 
namespace Webora\CustomGrid\Model\ResourceModel\Order\Grid;
 
use Magento\Sales\Model\ResourceModel\Order\Invoice\Orders\Grid\Collection as OriginalCollection;
 
class CollectionInvoice extends OriginalCollection{
 
    protected function _construct(){
        
       $this->addFilterToMap('country_id', 'soa.country_id');
       parent::_construct();
 
    }
 
    protected function _renderFiltersBefore(){
 
        $this->getSelect()->joinLeft(
            ["soa" => "sales_order_address"],
            'main_table.order_id = soa.parent_id',
            array('country_id')
        )
        ->distinct();
        
        
        parent::_renderFiltersBefore();
    }
}